


function cacheFunction(add) 
{
   if(!add)
   return;

   var obj={num1:0, num2:0, sum:0}
    
  

   return function invoking(a,b)
{
  if(a===obj.num1 && b===obj.num2)
  return obj.sum;

  else
  {
    var result= add(a,b);
    obj.num1=a;
    obj.num2=b;
    obj.sum=result;

    return result;
  }

}

}


module.exports=cacheFunction;