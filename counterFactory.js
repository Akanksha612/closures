

function counterFactory()
{

    var counter = 0;

    function changeBy(val)
    {

      if(val==undefined)
      return ;

      counter=counter+val;
    }


    return {
        increment: function() {
          changeBy(1);
          return counter;
        },
    
        decrement: function() {
          changeBy(-1);
          return counter;
        },
    }    

   

}

module.exports.counterFactory=counterFactory;

