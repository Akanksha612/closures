
var receive= require('../limitFunctionCallCount.js');

function add(a,b)
{
    return a+b;
}

const limitedAddFn = receive(add, 2);

console.log(limitedAddFn(1,2)); 
console.log(limitedAddFn(3,4)); 
console.log(limitedAddFn(4,5));





