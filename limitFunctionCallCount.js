

function limitFunctionCallCount(add, n)
{
    var count=0;

    if(!add || !n)
    return;

    return invoking=function (a,b)
    {
        count++;
        
        if(count<=n)
        return add(a,b);


    }
}



module.exports=limitFunctionCallCount;